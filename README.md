# Untar
## Cel:

Program rozpakowuje archiwa tar.gz i zwraca zawartość plików XML jako string.

## Opis kodu:

### Klasa TarManager:

**Metoda _Decompile_:**  
**Parametry:**  
- _filePath_(string): ścieżka do archiwum, które ma być rozpakowane.  
**Zwraca**: 
 (string): ścieżka do rozpakowanego pliku. 

### Klasa XMLManager:

**Metoda _GetContentOfXMLFile_:**  
**Parametry:**  
- _filePath_(string): ścieżka do pliku XML, z którego ma być pobrana zawartość.   
**Zwraca:**
 (string): zawartość pliku xml. 

## Przykład użycia:

```
using System;
using Untar;

namespace CheckUntar
{
    class Program
    {
        static void Main(string[] args)
        {
            string decompliedFile = TarManager
                .Decompress("/Users/daniel/Downloads/test.xml.gz");
            Console.WriteLine(XMLManager.GetContentOfXMLFile(decompliedFile));
        }
    }
}

```

## Autor
Kowalski Daniel
