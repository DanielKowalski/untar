﻿using System.IO;
using System.IO.Compression;

namespace Untar
{
    /// <summary>
    /// Menadżer do archiwów tar.gz
    /// </summary>
    public static class TarManager
    {
        /// <summary>
        /// Rozpakowuje archiwum .gz o podanej ścieżce.
        /// </summary>
        /// <returns>Ścieżkę do zdekompilowanego pliku.</returns>
        /// <param name="filePath">Ścieżka do archiwum do rozpakowania.</param>
        public static string Decompress(string filePath)
        {
            FileInfo fileToDecompress = new FileInfo(filePath);
            using (FileStream fileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(
                    currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream 
                       = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = 
                           new GZipStream(fileStream, 
                                          CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);

                    }
                }
                return newFileName;
            }
        }
    }
}
