﻿using System.Xml.XPath;

namespace Untar
{
    /// <summary>
    /// Menadżer do plików xml.
    /// </summary>
    public static class XMLManager
    {
        /// <summary>
        /// Pobiera zawartość pliku xml.
        /// </summary>
        /// <returns>Zawartość pliku xml jako string.</returns>
        /// <param name="filePath">Ścieżka do pliku xml.</param>
        public static string GetContentOfXMLFile(string filePath)
        {
            XPathDocument file = new XPathDocument(filePath);
            return file.CreateNavigator().OuterXml;
        }
    }
}
